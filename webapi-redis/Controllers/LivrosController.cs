﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace webapi_redis.Controllers
{
    [Route("api/[controller]")]
    public class LivrosController : Controller
    {
        static object connectLock = Guid.NewGuid(); //new object();

        [HttpGet]
        public IActionResult Index([FromServices]IDistributedCache cache)
        {
            Console.WriteLine("testando");
            Console.WriteLine(cache.ToString());
            //dependency injection do Redis

            Livro[] livros =
            {
                 new Livro(){ ID = 1, Nome = "Livro 1" }
                ,new Livro (){ ID = 2, Nome = "Livro 2" }
            };

            string valorJSON = null;// cache.GetString("livros4");

            if (valorJSON == null)
            {
            lock (connectLock)
                {
                    bool passou = false;
                    Console.WriteLine("testando3");
                    valorJSON = cache.GetString("livros4");
                    Console.WriteLine("testando4");

                    if (valorJSON == null)
                    {
                        Console.WriteLine("testando5");

                        passou = true;
                        DistributedCacheEntryOptions opcoesCache =
                                    new DistributedCacheEntryOptions();
                        opcoesCache.SetAbsoluteExpiration(
                            TimeSpan.FromSeconds(10));

                        Console.WriteLine("testando6");

                        valorJSON = JsonConvert.SerializeObject(livros);
                        cache.SetString("livros4", valorJSON, opcoesCache);
                        Console.WriteLine("testando7");

                    }
                    Console.WriteLine("testando8");

                    valorJSON = JsonConvert.SerializeObject(livros);
                    Console.WriteLine("testando9");

                }
            }
            Console.WriteLine("testando10");

            return Ok(valorJSON);
        }
    }
}